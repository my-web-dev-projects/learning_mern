const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");

require("../db/conn");
const User = require("../model/userSchema");

router.get('/', (req, res) => {
    res.send('Hello world from the server');
})

// Using Promises
// router.post('/register', (req, res) => {
//     const {name, email, phone, work, password, cpassword} = req.body;
//     if(!name || !email || !phone || !work || !password || !cpassword){
//         return res.status(422).json({error: "Please fill the required fields."});
//     }

//     User.findOne({email:email})
//     .then((userExist) => {
//         if (userExist) {
//             return res.status(422).json({error: "Email already exists"});
//         }
//         const user = new User({name, email, phone, work, password, cpassword});

//         user.save().then(() => {
//             res.status(201).json({message:"user registered successfully"});
//         }).catch((error) => res.status(500).json({error: "Failed to register"}));
//     }).catch(error => {
//         console.log(`Error: ${error}`);
//     })
// })

// Using async await
// Register Route
router.post('/register', async (req, res) => {
    const { name, email, phone, work, password, cpassword } = req.body;
    if (!name || !email || !phone || !work || !password || !cpassword) {
        return res.status(422).json({ error: "Please fill the required fields." });
    }
    try {
        const userExist = await User.findOne({ email: email });
        if (userExist) {
            return res.status(422).json({ error: "Email already exists" });
        } else if (password != cpassword) {
            return res.status(422).json({ error: "Passwords are not matching!" })
        } else {
            const user = new User({ name, email, phone, work, password, cpassword });
            await user.save();
            res.status(201).json({ message: "user registered successfully" });
        }
    } catch (error) {
        console.log(error);
    }
})

// Signin Route
router.post('/signin', async (req, res) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(400).json({ error: "Please fill the required fields." })
        }
        const userLogin = await User.findOne({ email: email });

        if (userLogin) {
            const isPassMatch = await bcrypt.compare(password, userLogin.password)
            const token = await userLogin.generateAuthToken();
            res.cookie("jwtoken", token, {
                expires:new Date(Date.now() + 25892000000),
                httpOnly:true
            })

            if (!isPassMatch) {
                res.status(400).json({ error: "Invalid user details" })
            } else {
                res.status(200).json({ message: "User signin successfully" })
            }
        } else {
            res.status(400).json({ error: "Invalid user details" })
        }


    } catch (error) {
        console.log(error);
    }
})

module.exports = router;