import React, { useState } from 'react'
import { PandaCEA } from '../panda-close-eyes-animation/pandaCEA'
// import Logo from "../../assets/logo.svg"
import "./styleLogin.css";
import NavBar from '../navBar/NavBar';
import { Link } from 'react-router-dom';

export const RegisterPage = () => {
    const [isEyesClose, setIsEyesClose] = useState(false);
    const [user, setUser] = useState({
        name: "", email: "", phone: "", password: "", cpassword: ""
    })

    const handleInputs = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        console.log("Input name:", name);
        console.log("Input value:", value);
        setUser({ ...user, [name]: value });
    }
    

    const handlePasswordFocus = () => {
        setIsEyesClose(true)
    }

    const handlePasswordBlur = () => {
        setIsEyesClose(false);
    };

    return (
        <>
            <NavBar activePage={"RegisterPage"} />
            <div className="reg-body">
                {/* <img src={Logo} alt="" /> */}
                <div className="reg-container">
                    <div>
                        <PandaCEA isEyesClose={isEyesClose} svgWidth={200} svgHeight={150} />
                    </div>
                    {/* <form> */}
                        <div className="form">
                            <div className="inputBox">
                                <input type="text" autoComplete='off' required name='name' value={user.name} onChange={handleInputs} />
                                <span>Name</span>
                            </div>
                            <div className="inputBox">
                                <input type="text" autoComplete='off' required name='email' value={user.email} onChange={handleInputs}/>
                                <span>E - mail</span>
                            </div>
                            <div className="inputBox">
                                <input type="tel" autoComplete='off' required name='phone' value={user.phone} onChange={handleInputs}/>
                                <span>Phone Number</span>
                            </div>
                            <div class="inputBox">
                                <input type="password" autoComplete='off' required name='password' value={user.password} onChange={handleInputs} onFocus={handlePasswordFocus} onBlur={handlePasswordBlur} />
                                <span>Password</span>
                            </div>
                            <div class="inputBox">
                                <input type="password" autoComplete='off' required name='cpassword' value={user.cpassword} onChange={handleInputs} onFocus={handlePasswordFocus} onBlur={handlePasswordBlur} />
                                <span>Confirm Password</span>
                            </div>
                            <button className="submit-btn">Register</button>
                        </div>
                    {/* </form> */}
                    <span>Already have an account? <Link className="style-links" to="/login"  >Login Here Buddy!</Link></span>
                </div>
            </div>
        </>
    )
}
