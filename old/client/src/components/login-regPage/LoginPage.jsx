import React, { useState } from 'react'
import { PandaCEA } from '../panda-close-eyes-animation/pandaCEA'
// import Logo from "../../assets/logo.svg"
import "./styleLogin.css";
import NavBar from '../navBar/NavBar';
import { Link } from 'react-router-dom';

export const LoginPage = () => {

    const [isEyesClose, setIsEyesClose] = useState(false);

    const handlePasswordFocus = () => {
        setIsEyesClose(true)
    }

    const handlePasswordBlur = () => {
        setIsEyesClose(false);
    };

    return (
        <>
        <NavBar activePage={"LoginPage"}/>
        <div className="reg-body">
            {/* <img src={Logo} alt="" /> */}
            <div className="reg-container">
                <div>
                    <PandaCEA isEyesClose={isEyesClose} svgWidth={300} svgHeight={250} />
                </div>
                <div className="form">
                    <div className="inputBox">
                        <input type="text" required />
                        <span>E - mail</span>
                    </div>
                    <div class="inputBox">
                        <input type="password" required onFocus={handlePasswordFocus} onBlur={handlePasswordBlur} />
                        <span>Password</span>
                    </div>
                    <button className="submit-btn">Login</button>
                </div>
                    <span>New User? <Link className="style-links" to = "/register">Register Here Buddy!</Link></span>
            </div>
        </div>
        </>
    )
}
