import { React } from "react";
import NavBar from "../navBar/NavBar";
import "./StyleAboutPage.css"

const AboutPage = () => {
    return (
        <>
            <NavBar activePage={"AboutPage"}/>
            <h1>Hello this is About Page</h1>
        </>
    )
}
export default AboutPage;