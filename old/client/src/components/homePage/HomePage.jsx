import { React } from 'react';
import NavBar from '../navBar/NavBar';
import './StyleHomePage.css';

const HomePage = () => {
    return (
        <>
            <NavBar activePage={"HomePage"}/>
            <h1>Hello HomePage</h1>
        </>
    )
}

export default HomePage