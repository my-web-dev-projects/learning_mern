import { React } from 'react';
import NavBar from '../navBar/NavBar';
import './StyleContactPage.css'

const ContactPage = () => {
    return (
        <>
            <NavBar activePage={"ContactPage"}/>
            <h1>Hello Contact Page</h1>
        </>
    )
}

export default ContactPage;