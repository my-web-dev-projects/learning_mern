import React from 'react'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import NavBar from './components/navBar/NavBar'
import HomePage from  './components/homePage/HomePage'
import ErrorPage from './components/errorPage/ErrorPage'
import AboutPage from './components/aboutPage/AboutPage'
import ContactPage from './components/contactPage/ContactPage'
import {LoginPage} from './components/login-regPage/LoginPage'
import {RegisterPage} from './components/login-regPage/RegisterPage'

const App = () => {
  const router = createBrowserRouter([
    {
      path:'/',
      element: <HomePage/>,
      errorElement: <ErrorPage/>
    },
    {
      path:'/about',
      element: <AboutPage/>,
      errorElement: <ErrorPage/>
    },
    {
      path:'/contact',
      element: <ContactPage/>,
      errorElement: <ErrorPage/>
    },
    {
      path:'/login',
      element: <LoginPage/>,
      errorElement: <ErrorPage/>
    },
    {
      path:'/register',
      element: <RegisterPage/>,
      errorElement: <ErrorPage/>
    },
  ])
  return (
    <>
    <RouterProvider router={router}/>
    </>
  )
}

export default App